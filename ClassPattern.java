
public class ClassPattern {

	public static void main(String[] args) {
		int size = 3;
		int i;
		int j;
		for (i = size; i >= -size; size--) {
			for (j = Math.abs(i); j >= -size; j++) {

				System.out.print((char) (j + 65));
			}
			System.out.print("\n");
		}

	}
}