

import java.util.Scanner;

public class ThirdWayPrimeNumber {

    public static void main(String[] args) {
    	Scanner scan = new Scanner (System.in);
		System.out.print("Enter the number : ");
		int num = scan.nextInt();
		if(num == 0||num == 1){   
        	System.out.println(num+" is not prime number");  
        	}
		else {
        boolean prime = true;
        for(int i = 2; i <num; i++){
      
            if(num % i == 0){
                prime = false;
                break;
            }
        }
        if (prime)
            System.out.println(num + " is a prime number.");
        else
            System.out.println(num + " is not a prime number.");
		
		}
    }
}


